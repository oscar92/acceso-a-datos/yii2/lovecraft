<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "objeto".
 *
 * @property int $codigo_objeto
 * @property int $codigo_autor
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property string|null $uso
 * @property string|null $consecuencia
 *
 * @property Autores $codigoAutor
 * @property RelatoObjeto[] $relatoObjetos
 * @property Relatos[] $codigoRelatos
 */
class Objeto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objeto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_autor'], 'required'],
            [['codigo_autor'], 'integer'],
            [['nombre', 'descripcion', 'uso', 'consecuencia'], 'string', 'max' => 30],
            [['codigo_autor'], 'exist', 'skipOnError' => true, 'targetClass' => Autores::className(), 'targetAttribute' => ['codigo_autor' => 'codigo_autor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_objeto' => 'Codigo Objeto',
            'codigo_autor' => 'Codigo Autor',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'uso' => 'Uso',
            'consecuencia' => 'Consecuencia',
        ];
    }

    /**
     * Gets query for [[CodigoAutor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAutor()
    {
        return $this->hasOne(Autores::className(), ['codigo_autor' => 'codigo_autor']);
    }

    /**
     * Gets query for [[RelatoObjetos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRelatoObjetos()
    {
        return $this->hasMany(RelatoObjeto::className(), ['codigo_objeto' => 'codigo_objeto']);
    }

    /**
     * Gets query for [[CodigoRelatos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoRelatos()
    {
        return $this->hasMany(Relatos::className(), ['codigo_relato' => 'codigo_relato'])->viaTable('relato_objeto', ['codigo_objeto' => 'codigo_objeto']);
    }
}
