<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "relato_deidad".
 *
 * @property int $codigo_relato
 * @property int $codigo_deidad
 *
 * @property Deidades $codigoDeidad
 * @property Relatos $codigoRelato
 */
class RelatoDeidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'relato_deidad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_relato', 'codigo_deidad'], 'required'],
            [['codigo_relato', 'codigo_deidad'], 'integer'],
            [['codigo_relato', 'codigo_deidad'], 'unique', 'targetAttribute' => ['codigo_relato', 'codigo_deidad']],
            [['codigo_deidad'], 'exist', 'skipOnError' => true, 'targetClass' => Deidades::className(), 'targetAttribute' => ['codigo_deidad' => 'codigo_deidad']],
            [['codigo_relato'], 'exist', 'skipOnError' => true, 'targetClass' => Relatos::className(), 'targetAttribute' => ['codigo_relato' => 'codigo_relato']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_relato' => 'Codigo Relato',
            'codigo_deidad' => 'Codigo Deidad',
        ];
    }

    /**
     * Gets query for [[CodigoDeidad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDeidad()
    {
        return $this->hasOne(Deidades::className(), ['codigo_deidad' => 'codigo_deidad']);
    }

    /**
     * Gets query for [[CodigoRelato]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoRelato()
    {
        return $this->hasOne(Relatos::className(), ['codigo_relato' => 'codigo_relato']);
    }
}
