<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "relato_ubicacion".
 *
 * @property int $codigo_relato
 * @property int $codigo_ubicacion
 *
 * @property Relatos $codigoRelato
 * @property Ubicacion $codigoUbicacion
 */
class RelatoUbicacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'relato_ubicacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_relato', 'codigo_ubicacion'], 'required'],
            [['codigo_relato', 'codigo_ubicacion'], 'integer'],
            [['codigo_relato', 'codigo_ubicacion'], 'unique', 'targetAttribute' => ['codigo_relato', 'codigo_ubicacion']],
            [['codigo_relato'], 'exist', 'skipOnError' => true, 'targetClass' => Relatos::className(), 'targetAttribute' => ['codigo_relato' => 'codigo_relato']],
            [['codigo_ubicacion'], 'exist', 'skipOnError' => true, 'targetClass' => Ubicacion::className(), 'targetAttribute' => ['codigo_ubicacion' => 'codigo_ubicacion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_relato' => 'Codigo Relato',
            'codigo_ubicacion' => 'Codigo Ubicacion',
        ];
    }

    /**
     * Gets query for [[CodigoRelato]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoRelato()
    {
        return $this->hasOne(Relatos::className(), ['codigo_relato' => 'codigo_relato']);
    }

    /**
     * Gets query for [[CodigoUbicacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoUbicacion()
    {
        return $this->hasOne(Ubicacion::className(), ['codigo_ubicacion' => 'codigo_ubicacion']);
    }
}
