<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "relatos".
 *
 * @property int $codigo_relato
 * @property int $codigo_autor
 * @property string|null $nombre
 * @property string|null $fecha_publicacion
 *
 * @property RelatoDeidad[] $relatoDeidads
 * @property Deidades[] $codigoDeidads
 * @property RelatoMonstruo[] $relatoMonstruos
 * @property Monstruos[] $codigoMonstruos
 * @property RelatoObjeto[] $relatoObjetos
 * @property Objeto[] $codigoObjetos
 * @property RelatoUbicacion[] $relatoUbicacions
 * @property Ubicacion[] $codigoUbicacions
 * @property Autores $codigoAutor
 */
class Relatos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'relatos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_autor'], 'required'],
            [['codigo_autor'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['fecha_publicacion'], 'string', 'max' => 4],
            [['codigo_autor'], 'exist', 'skipOnError' => true, 'targetClass' => Autores::className(), 'targetAttribute' => ['codigo_autor' => 'codigo_autor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_relato' => 'Codigo Relato',
            'codigo_autor' => 'Codigo Autor',
            'nombre' => 'Nombre',
            'fecha_publicacion' => 'Fecha Publicacion',
        ];
    }

    /**
     * Gets query for [[RelatoDeidads]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRelatoDeidads()
    {
        return $this->hasMany(RelatoDeidad::className(), ['codigo_relato' => 'codigo_relato']);
    }

    /**
     * Gets query for [[CodigoDeidads]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDeidads()
    {
        return $this->hasMany(Deidades::className(), ['codigo_deidad' => 'codigo_deidad'])->viaTable('relato_deidad', ['codigo_relato' => 'codigo_relato']);
    }

    /**
     * Gets query for [[RelatoMonstruos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRelatoMonstruos()
    {
        return $this->hasMany(RelatoMonstruo::className(), ['codigo_relato' => 'codigo_relato']);
    }

    /**
     * Gets query for [[CodigoMonstruos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMonstruos()
    {
        return $this->hasMany(Monstruos::className(), ['codigo_monstruo' => 'codigo_monstruo'])->viaTable('relato_monstruo', ['codigo_relato' => 'codigo_relato']);
    }

    /**
     * Gets query for [[RelatoObjetos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRelatoObjetos()
    {
        return $this->hasMany(RelatoObjeto::className(), ['codigo_relato' => 'codigo_relato']);
    }

    /**
     * Gets query for [[CodigoObjetos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoObjetos()
    {
        return $this->hasMany(Objeto::className(), ['codigo_objeto' => 'codigo_objeto'])->viaTable('relato_objeto', ['codigo_relato' => 'codigo_relato']);
    }

    /**
     * Gets query for [[RelatoUbicacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRelatoUbicacions()
    {
        return $this->hasMany(RelatoUbicacion::className(), ['codigo_relato' => 'codigo_relato']);
    }

    /**
     * Gets query for [[CodigoUbicacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoUbicacions()
    {
        return $this->hasMany(Ubicacion::className(), ['codigo_ubicacion' => 'codigo_ubicacion'])->viaTable('relato_ubicacion', ['codigo_relato' => 'codigo_relato']);
    }

    /**
     * Gets query for [[CodigoAutor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAutor()
    {
        return $this->hasOne(Autores::className(), ['codigo_autor' => 'codigo_autor']);
    }
}
