<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ubicacion".
 *
 * @property int $codigo_ubicacion
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property string|null $esReal
 * @property string|null $localizacion
 *
 * @property RelatoUbicacion[] $relatoUbicacions
 * @property Relatos[] $codigoRelatos
 */
class Ubicacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ubicacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'localizacion'], 'string', 'max' => 30],
            [['descripcion'], 'string', 'max' => 50],
            [['esReal'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_ubicacion' => 'Codigo Ubicacion',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'esReal' => 'Es Real',
            'localizacion' => 'Localizacion',
        ];
    }

    /**
     * Gets query for [[RelatoUbicacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRelatoUbicacions()
    {
        return $this->hasMany(RelatoUbicacion::className(), ['codigo_ubicacion' => 'codigo_ubicacion']);
    }

    /**
     * Gets query for [[CodigoRelatos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoRelatos()
    {
        return $this->hasMany(Relatos::className(), ['codigo_relato' => 'codigo_relato'])->viaTable('relato_ubicacion', ['codigo_ubicacion' => 'codigo_ubicacion']);
    }
}
