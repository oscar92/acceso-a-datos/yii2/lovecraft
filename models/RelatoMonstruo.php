<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "relato_monstruo".
 *
 * @property int $codigo_relato
 * @property int $codigo_monstruo
 *
 * @property Monstruos $codigoMonstruo
 * @property Relatos $codigoRelato
 */
class RelatoMonstruo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'relato_monstruo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_relato', 'codigo_monstruo'], 'required'],
            [['codigo_relato', 'codigo_monstruo'], 'integer'],
            [['codigo_relato', 'codigo_monstruo'], 'unique', 'targetAttribute' => ['codigo_relato', 'codigo_monstruo']],
            [['codigo_monstruo'], 'exist', 'skipOnError' => true, 'targetClass' => Monstruos::className(), 'targetAttribute' => ['codigo_monstruo' => 'codigo_monstruo']],
            [['codigo_relato'], 'exist', 'skipOnError' => true, 'targetClass' => Relatos::className(), 'targetAttribute' => ['codigo_relato' => 'codigo_relato']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_relato' => 'Codigo Relato',
            'codigo_monstruo' => 'Codigo Monstruo',
        ];
    }

    /**
     * Gets query for [[CodigoMonstruo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMonstruo()
    {
        return $this->hasOne(Monstruos::className(), ['codigo_monstruo' => 'codigo_monstruo']);
    }

    /**
     * Gets query for [[CodigoRelato]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoRelato()
    {
        return $this->hasOne(Relatos::className(), ['codigo_relato' => 'codigo_relato']);
    }
}
