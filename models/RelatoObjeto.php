<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "relato_objeto".
 *
 * @property int $codigo_relato
 * @property int $codigo_objeto
 *
 * @property Objeto $codigoObjeto
 * @property Relatos $codigoRelato
 */
class RelatoObjeto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'relato_objeto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_relato', 'codigo_objeto'], 'required'],
            [['codigo_relato', 'codigo_objeto'], 'integer'],
            [['codigo_relato', 'codigo_objeto'], 'unique', 'targetAttribute' => ['codigo_relato', 'codigo_objeto']],
            [['codigo_objeto'], 'exist', 'skipOnError' => true, 'targetClass' => Objeto::className(), 'targetAttribute' => ['codigo_objeto' => 'codigo_objeto']],
            [['codigo_relato'], 'exist', 'skipOnError' => true, 'targetClass' => Relatos::className(), 'targetAttribute' => ['codigo_relato' => 'codigo_relato']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_relato' => 'Codigo Relato',
            'codigo_objeto' => 'Codigo Objeto',
        ];
    }

    /**
     * Gets query for [[CodigoObjeto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoObjeto()
    {
        return $this->hasOne(Objeto::className(), ['codigo_objeto' => 'codigo_objeto']);
    }

    /**
     * Gets query for [[CodigoRelato]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoRelato()
    {
        return $this->hasOne(Relatos::className(), ['codigo_relato' => 'codigo_relato']);
    }
}
