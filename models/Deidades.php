<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "deidades".
 *
 * @property int $codigo_deidad
 * @property int $codigo_autor
 * @property string|null $nombre
 * @property string|null $clase
 *
 * @property Autores $codigoAutor
 * @property Epitetos[] $epitetos
 * @property Epitetos[] $epitetos0
 * @property RelatoDeidad[] $relatoDeidads
 * @property Relatos[] $codigoRelatos
 */
class Deidades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deidades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_autor'], 'required'],
            [['codigo_autor'], 'integer'],
            [['nombre', 'clase'], 'string', 'max' => 30],
            [['codigo_autor'], 'exist', 'skipOnError' => true, 'targetClass' => Autores::className(), 'targetAttribute' => ['codigo_autor' => 'codigo_autor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_deidad' => 'Codigo Deidad',
            'codigo_autor' => 'Codigo Autor',
            'nombre' => 'Nombre',
            'clase' => 'Clase',
        ];
    }

    /**
     * Gets query for [[CodigoAutor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAutor()
    {
        return $this->hasOne(Autores::className(), ['codigo_autor' => 'codigo_autor']);
    }

    /**
     * Gets query for [[Epitetos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEpitetos()
    {
        return $this->hasMany(Epitetos::className(), ['codigo_autor' => 'codigo_autor']);
    }

    /**
     * Gets query for [[Epitetos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEpitetos0()
    {
        return $this->hasMany(Epitetos::className(), ['codigo_deidad' => 'codigo_deidad']);
    }

    /**
     * Gets query for [[RelatoDeidads]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRelatoDeidads()
    {
        return $this->hasMany(RelatoDeidad::className(), ['codigo_deidad' => 'codigo_deidad']);
    }

    /**
     * Gets query for [[CodigoRelatos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoRelatos()
    {
        return $this->hasMany(Relatos::className(), ['codigo_relato' => 'codigo_relato'])->viaTable('relato_deidad', ['codigo_deidad' => 'codigo_deidad']);
    }
}
