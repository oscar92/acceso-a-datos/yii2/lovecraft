<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autores".
 *
 * @property int $codigo_autor
 * @property string|null $nombre
 * @property string|null $nacionalidad
 *
 * @property Deidades[] $deidades
 * @property Monstruos[] $monstruos
 * @property Objeto[] $objetos
 * @property Relatos[] $relatos
 */
class Autores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'nacionalidad'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_autor' => 'Codigo Autor',
            'nombre' => 'Nombre',
            'nacionalidad' => 'Nacionalidad',
        ];
    }

    /**
     * Gets query for [[Deidades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDeidades()
    {
        return $this->hasMany(Deidades::className(), ['codigo_autor' => 'codigo_autor']);
    }

    /**
     * Gets query for [[Monstruos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMonstruos()
    {
        return $this->hasMany(Monstruos::className(), ['codigo_autor' => 'codigo_autor']);
    }

    /**
     * Gets query for [[Objetos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjetos()
    {
        return $this->hasMany(Objeto::className(), ['codigo_autor' => 'codigo_autor']);
    }

    /**
     * Gets query for [[Relatos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRelatos()
    {
        return $this->hasMany(Relatos::className(), ['codigo_autor' => 'codigo_autor']);
    }
}
