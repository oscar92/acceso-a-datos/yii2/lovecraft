<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monstruos".
 *
 * @property int $codigo_monstruo
 * @property int $codigo_autor
 * @property string|null $nombre
 * @property string|null $clasificacion
 *
 * @property Autores $codigoAutor
 * @property RelatoMonstruo[] $relatoMonstruos
 * @property Relatos[] $codigoRelatos
 */
class Monstruos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'monstruos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_autor'], 'required'],
            [['codigo_autor'], 'integer'],
            [['nombre', 'clasificacion'], 'string', 'max' => 30],
            [['codigo_autor'], 'exist', 'skipOnError' => true, 'targetClass' => Autores::className(), 'targetAttribute' => ['codigo_autor' => 'codigo_autor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_monstruo' => 'Codigo Monstruo',
            'codigo_autor' => 'Codigo Autor',
            'nombre' => 'Nombre',
            'clasificacion' => 'Clasificacion',
        ];
    }

    /**
     * Gets query for [[CodigoAutor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAutor()
    {
        return $this->hasOne(Autores::className(), ['codigo_autor' => 'codigo_autor']);
    }

    /**
     * Gets query for [[RelatoMonstruos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRelatoMonstruos()
    {
        return $this->hasMany(RelatoMonstruo::className(), ['codigo_monstruo' => 'codigo_monstruo']);
    }

    /**
     * Gets query for [[CodigoRelatos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoRelatos()
    {
        return $this->hasMany(Relatos::className(), ['codigo_relato' => 'codigo_relato'])->viaTable('relato_monstruo', ['codigo_monstruo' => 'codigo_monstruo']);
    }
}
