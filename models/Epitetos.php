<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "epitetos".
 *
 * @property string $epitetos
 * @property int $codigo_deidad
 * @property int $codigo_autor
 *
 * @property Deidades $codigoAutor
 * @property Deidades $codigoDeidad
 */
class Epitetos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'epitetos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['epitetos', 'codigo_deidad', 'codigo_autor'], 'required'],
            [['codigo_deidad', 'codigo_autor'], 'integer'],
            [['epitetos'], 'string', 'max' => 30],
            [['epitetos', 'codigo_deidad', 'codigo_autor'], 'unique', 'targetAttribute' => ['epitetos', 'codigo_deidad', 'codigo_autor']],
            [['codigo_autor'], 'exist', 'skipOnError' => true, 'targetClass' => Deidades::className(), 'targetAttribute' => ['codigo_autor' => 'codigo_autor']],
            [['codigo_deidad'], 'exist', 'skipOnError' => true, 'targetClass' => Deidades::className(), 'targetAttribute' => ['codigo_deidad' => 'codigo_deidad']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'epitetos' => 'Epitetos',
            'codigo_deidad' => 'Codigo Deidad',
            'codigo_autor' => 'Codigo Autor',
        ];
    }

    /**
     * Gets query for [[CodigoAutor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAutor()
    {
        return $this->hasOne(Deidades::className(), ['codigo_autor' => 'codigo_autor']);
    }

    /**
     * Gets query for [[CodigoDeidad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDeidad()
    {
        return $this->hasOne(Deidades::className(), ['codigo_deidad' => 'codigo_deidad']);
    }
}
