<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Monstruos */

$this->title = 'Create Monstruos';
$this->params['breadcrumbs'][] = ['label' => 'Monstruos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monstruos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
