<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Monstruos */

$this->title = $model->codigo_monstruo;
$this->params['breadcrumbs'][] = ['label' => 'Monstruos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="monstruos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigo_monstruo' => $model->codigo_monstruo, 'codigo_autor' => $model->codigo_autor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigo_monstruo' => $model->codigo_monstruo, 'codigo_autor' => $model->codigo_autor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_monstruo',
            'codigo_autor',
            'nombre',
            'clasificacion',
        ],
    ]) ?>

</div>
