<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Monstruos */

$this->title = 'Update Monstruos: ' . $model->codigo_monstruo;
$this->params['breadcrumbs'][] = ['label' => 'Monstruos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_monstruo, 'url' => ['view', 'codigo_monstruo' => $model->codigo_monstruo, 'codigo_autor' => $model->codigo_autor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="monstruos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
