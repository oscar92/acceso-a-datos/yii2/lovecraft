<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Monstruos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monstruos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Monstruos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_monstruo',
            'codigo_autor',
            'nombre',
            'clasificacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
