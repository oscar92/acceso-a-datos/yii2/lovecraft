<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Relato Ubicacions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="relato-ubicacion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Relato Ubicacion', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_relato',
            'codigo_ubicacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
