<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RelatoUbicacion */

$this->title = 'Update Relato Ubicacion: ' . $model->codigo_relato;
$this->params['breadcrumbs'][] = ['label' => 'Relato Ubicacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_relato, 'url' => ['view', 'codigo_relato' => $model->codigo_relato, 'codigo_ubicacion' => $model->codigo_ubicacion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="relato-ubicacion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
