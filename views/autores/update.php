<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Autores */

$this->title = 'Update Autores: ' . $model->codigo_autor;
$this->params['breadcrumbs'][] = ['label' => 'Autores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_autor, 'url' => ['view', 'id' => $model->codigo_autor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="autores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
