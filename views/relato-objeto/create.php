<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RelatoObjeto */

$this->title = 'Create Relato Objeto';
$this->params['breadcrumbs'][] = ['label' => 'Relato Objetos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="relato-objeto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
