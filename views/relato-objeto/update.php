<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RelatoObjeto */

$this->title = 'Update Relato Objeto: ' . $model->codigo_relato;
$this->params['breadcrumbs'][] = ['label' => 'Relato Objetos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_relato, 'url' => ['view', 'codigo_relato' => $model->codigo_relato, 'codigo_objeto' => $model->codigo_objeto]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="relato-objeto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
