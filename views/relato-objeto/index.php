<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Relato Objetos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="relato-objeto-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Relato Objeto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_relato',
            'codigo_objeto',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
