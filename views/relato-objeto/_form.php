<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RelatoObjeto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="relato-objeto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_relato')->textInput() ?>

    <?= $form->field($model, 'codigo_objeto')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
