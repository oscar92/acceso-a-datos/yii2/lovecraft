<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Epitetos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="epitetos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Epitetos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'epitetos',
            'codigo_deidad',
            'codigo_autor',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
