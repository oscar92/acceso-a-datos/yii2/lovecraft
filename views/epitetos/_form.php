<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Epitetos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="epitetos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'epitetos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_deidad')->textInput() ?>

    <?= $form->field($model, 'codigo_autor')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
