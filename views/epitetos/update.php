<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Epitetos */

$this->title = 'Update Epitetos: ' . $model->epitetos;
$this->params['breadcrumbs'][] = ['label' => 'Epitetos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->epitetos, 'url' => ['view', 'epitetos' => $model->epitetos, 'codigo_deidad' => $model->codigo_deidad, 'codigo_autor' => $model->codigo_autor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="epitetos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
