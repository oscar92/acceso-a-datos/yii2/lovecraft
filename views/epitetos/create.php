<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Epitetos */

$this->title = 'Create Epitetos';
$this->params['breadcrumbs'][] = ['label' => 'Epitetos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="epitetos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
