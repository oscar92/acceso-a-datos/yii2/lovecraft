<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Epitetos */

$this->title = $model->epitetos;
$this->params['breadcrumbs'][] = ['label' => 'Epitetos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="epitetos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'epitetos' => $model->epitetos, 'codigo_deidad' => $model->codigo_deidad, 'codigo_autor' => $model->codigo_autor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'epitetos' => $model->epitetos, 'codigo_deidad' => $model->codigo_deidad, 'codigo_autor' => $model->codigo_autor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'epitetos',
            'codigo_deidad',
            'codigo_autor',
        ],
    ]) ?>

</div>
