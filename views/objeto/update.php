<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Objeto */

$this->title = 'Update Objeto: ' . $model->codigo_objeto;
$this->params['breadcrumbs'][] = ['label' => 'Objetos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_objeto, 'url' => ['view', 'codigo_objeto' => $model->codigo_objeto, 'codigo_autor' => $model->codigo_autor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="objeto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
