<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RelatoDeidad */

$this->title = 'Update Relato Deidad: ' . $model->codigo_relato;
$this->params['breadcrumbs'][] = ['label' => 'Relato Deidads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_relato, 'url' => ['view', 'codigo_relato' => $model->codigo_relato, 'codigo_deidad' => $model->codigo_deidad]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="relato-deidad-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
