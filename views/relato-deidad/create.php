<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RelatoDeidad */

$this->title = 'Create Relato Deidad';
$this->params['breadcrumbs'][] = ['label' => 'Relato Deidads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="relato-deidad-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
