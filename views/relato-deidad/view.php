<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RelatoDeidad */

$this->title = $model->codigo_relato;
$this->params['breadcrumbs'][] = ['label' => 'Relato Deidads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="relato-deidad-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigo_relato' => $model->codigo_relato, 'codigo_deidad' => $model->codigo_deidad], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigo_relato' => $model->codigo_relato, 'codigo_deidad' => $model->codigo_deidad], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_relato',
            'codigo_deidad',
        ],
    ]) ?>

</div>
