<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Deidades */

$this->title = 'Update Deidades: ' . $model->codigo_deidad;
$this->params['breadcrumbs'][] = ['label' => 'Deidades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_deidad, 'url' => ['view', 'codigo_deidad' => $model->codigo_deidad, 'codigo_autor' => $model->codigo_autor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="deidades-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
