<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Deidades */

$this->title = $model->codigo_deidad;
$this->params['breadcrumbs'][] = ['label' => 'Deidades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="deidades-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigo_deidad' => $model->codigo_deidad, 'codigo_autor' => $model->codigo_autor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigo_deidad' => $model->codigo_deidad, 'codigo_autor' => $model->codigo_autor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_deidad',
            'codigo_autor',
            'nombre',
            'clase',
        ],
    ]) ?>

</div>
