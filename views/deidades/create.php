<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Deidades */

$this->title = 'Create Deidades';
$this->params['breadcrumbs'][] = ['label' => 'Deidades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deidades-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
