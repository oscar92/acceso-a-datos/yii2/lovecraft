<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Relatos */

$this->title = 'Create Relatos';
$this->params['breadcrumbs'][] = ['label' => 'Relatos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="relatos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
