<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Relatos */

$this->title = 'Update Relatos: ' . $model->codigo_relato;
$this->params['breadcrumbs'][] = ['label' => 'Relatos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_relato, 'url' => ['view', 'codigo_relato' => $model->codigo_relato, 'codigo_autor' => $model->codigo_autor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="relatos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
