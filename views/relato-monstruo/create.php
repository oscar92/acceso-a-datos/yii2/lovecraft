<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RelatoMonstruo */

$this->title = 'Create Relato Monstruo';
$this->params['breadcrumbs'][] = ['label' => 'Relato Monstruos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="relato-monstruo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
