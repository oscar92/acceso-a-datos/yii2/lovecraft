<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RelatoMonstruo */

$this->title = $model->codigo_relato;
$this->params['breadcrumbs'][] = ['label' => 'Relato Monstruos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="relato-monstruo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigo_relato' => $model->codigo_relato, 'codigo_monstruo' => $model->codigo_monstruo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigo_relato' => $model->codigo_relato, 'codigo_monstruo' => $model->codigo_monstruo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_relato',
            'codigo_monstruo',
        ],
    ]) ?>

</div>
