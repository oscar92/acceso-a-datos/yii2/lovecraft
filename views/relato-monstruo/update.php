<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RelatoMonstruo */

$this->title = 'Update Relato Monstruo: ' . $model->codigo_relato;
$this->params['breadcrumbs'][] = ['label' => 'Relato Monstruos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_relato, 'url' => ['view', 'codigo_relato' => $model->codigo_relato, 'codigo_monstruo' => $model->codigo_monstruo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="relato-monstruo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
