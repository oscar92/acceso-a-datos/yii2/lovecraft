<?php

namespace app\controllers;

use Yii;
use app\models\Objeto;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ObjetoController implements the CRUD actions for Objeto model.
 */
class ObjetoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Objeto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Objeto::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Objeto model.
     * @param integer $codigo_objeto
     * @param integer $codigo_autor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_objeto, $codigo_autor)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_objeto, $codigo_autor),
        ]);
    }

    /**
     * Creates a new Objeto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Objeto();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_objeto' => $model->codigo_objeto, 'codigo_autor' => $model->codigo_autor]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Objeto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_objeto
     * @param integer $codigo_autor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_objeto, $codigo_autor)
    {
        $model = $this->findModel($codigo_objeto, $codigo_autor);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_objeto' => $model->codigo_objeto, 'codigo_autor' => $model->codigo_autor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Objeto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_objeto
     * @param integer $codigo_autor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_objeto, $codigo_autor)
    {
        $this->findModel($codigo_objeto, $codigo_autor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Objeto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_objeto
     * @param integer $codigo_autor
     * @return Objeto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_objeto, $codigo_autor)
    {
        if (($model = Objeto::findOne(['codigo_objeto' => $codigo_objeto, 'codigo_autor' => $codigo_autor])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
