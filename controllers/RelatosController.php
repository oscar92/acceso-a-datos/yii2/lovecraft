<?php

namespace app\controllers;

use Yii;
use app\models\Relatos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RelatosController implements the CRUD actions for Relatos model.
 */
class RelatosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Relatos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Relatos::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Relatos model.
     * @param integer $codigo_relato
     * @param integer $codigo_autor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_relato, $codigo_autor)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_relato, $codigo_autor),
        ]);
    }

    /**
     * Creates a new Relatos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Relatos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_relato' => $model->codigo_relato, 'codigo_autor' => $model->codigo_autor]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Relatos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_relato
     * @param integer $codigo_autor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_relato, $codigo_autor)
    {
        $model = $this->findModel($codigo_relato, $codigo_autor);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_relato' => $model->codigo_relato, 'codigo_autor' => $model->codigo_autor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Relatos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_relato
     * @param integer $codigo_autor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_relato, $codigo_autor)
    {
        $this->findModel($codigo_relato, $codigo_autor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Relatos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_relato
     * @param integer $codigo_autor
     * @return Relatos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_relato, $codigo_autor)
    {
        if (($model = Relatos::findOne(['codigo_relato' => $codigo_relato, 'codigo_autor' => $codigo_autor])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
