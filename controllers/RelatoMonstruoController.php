<?php

namespace app\controllers;

use Yii;
use app\models\RelatoMonstruo;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RelatoMonstruoController implements the CRUD actions for RelatoMonstruo model.
 */
class RelatoMonstruoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RelatoMonstruo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RelatoMonstruo::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RelatoMonstruo model.
     * @param integer $codigo_relato
     * @param integer $codigo_monstruo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_relato, $codigo_monstruo)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_relato, $codigo_monstruo),
        ]);
    }

    /**
     * Creates a new RelatoMonstruo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RelatoMonstruo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_relato' => $model->codigo_relato, 'codigo_monstruo' => $model->codigo_monstruo]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RelatoMonstruo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_relato
     * @param integer $codigo_monstruo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_relato, $codigo_monstruo)
    {
        $model = $this->findModel($codigo_relato, $codigo_monstruo);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_relato' => $model->codigo_relato, 'codigo_monstruo' => $model->codigo_monstruo]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RelatoMonstruo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_relato
     * @param integer $codigo_monstruo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_relato, $codigo_monstruo)
    {
        $this->findModel($codigo_relato, $codigo_monstruo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RelatoMonstruo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_relato
     * @param integer $codigo_monstruo
     * @return RelatoMonstruo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_relato, $codigo_monstruo)
    {
        if (($model = RelatoMonstruo::findOne(['codigo_relato' => $codigo_relato, 'codigo_monstruo' => $codigo_monstruo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
