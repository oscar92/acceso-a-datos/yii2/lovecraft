<?php

namespace app\controllers;

use Yii;
use app\models\RelatoObjeto;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RelatoObjetoController implements the CRUD actions for RelatoObjeto model.
 */
class RelatoObjetoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RelatoObjeto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RelatoObjeto::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RelatoObjeto model.
     * @param integer $codigo_relato
     * @param integer $codigo_objeto
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_relato, $codigo_objeto)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_relato, $codigo_objeto),
        ]);
    }

    /**
     * Creates a new RelatoObjeto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RelatoObjeto();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_relato' => $model->codigo_relato, 'codigo_objeto' => $model->codigo_objeto]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RelatoObjeto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_relato
     * @param integer $codigo_objeto
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_relato, $codigo_objeto)
    {
        $model = $this->findModel($codigo_relato, $codigo_objeto);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_relato' => $model->codigo_relato, 'codigo_objeto' => $model->codigo_objeto]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RelatoObjeto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_relato
     * @param integer $codigo_objeto
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_relato, $codigo_objeto)
    {
        $this->findModel($codigo_relato, $codigo_objeto)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RelatoObjeto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_relato
     * @param integer $codigo_objeto
     * @return RelatoObjeto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_relato, $codigo_objeto)
    {
        if (($model = RelatoObjeto::findOne(['codigo_relato' => $codigo_relato, 'codigo_objeto' => $codigo_objeto])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
