<?php

namespace app\controllers;

use Yii;
use app\models\RelatoUbicacion;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RelatoUbicacionController implements the CRUD actions for RelatoUbicacion model.
 */
class RelatoUbicacionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RelatoUbicacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RelatoUbicacion::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RelatoUbicacion model.
     * @param integer $codigo_relato
     * @param integer $codigo_ubicacion
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_relato, $codigo_ubicacion)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_relato, $codigo_ubicacion),
        ]);
    }

    /**
     * Creates a new RelatoUbicacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RelatoUbicacion();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_relato' => $model->codigo_relato, 'codigo_ubicacion' => $model->codigo_ubicacion]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RelatoUbicacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_relato
     * @param integer $codigo_ubicacion
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_relato, $codigo_ubicacion)
    {
        $model = $this->findModel($codigo_relato, $codigo_ubicacion);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_relato' => $model->codigo_relato, 'codigo_ubicacion' => $model->codigo_ubicacion]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RelatoUbicacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_relato
     * @param integer $codigo_ubicacion
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_relato, $codigo_ubicacion)
    {
        $this->findModel($codigo_relato, $codigo_ubicacion)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RelatoUbicacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_relato
     * @param integer $codigo_ubicacion
     * @return RelatoUbicacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_relato, $codigo_ubicacion)
    {
        if (($model = RelatoUbicacion::findOne(['codigo_relato' => $codigo_relato, 'codigo_ubicacion' => $codigo_ubicacion])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
