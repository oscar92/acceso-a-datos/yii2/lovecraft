<?php

namespace app\controllers;

use Yii;
use app\models\Epitetos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EpitetosController implements the CRUD actions for Epitetos model.
 */
class EpitetosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Epitetos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Epitetos::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Epitetos model.
     * @param string $epitetos
     * @param integer $codigo_deidad
     * @param integer $codigo_autor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($epitetos, $codigo_deidad, $codigo_autor)
    {
        return $this->render('view', [
            'model' => $this->findModel($epitetos, $codigo_deidad, $codigo_autor),
        ]);
    }

    /**
     * Creates a new Epitetos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Epitetos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'epitetos' => $model->epitetos, 'codigo_deidad' => $model->codigo_deidad, 'codigo_autor' => $model->codigo_autor]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Epitetos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $epitetos
     * @param integer $codigo_deidad
     * @param integer $codigo_autor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($epitetos, $codigo_deidad, $codigo_autor)
    {
        $model = $this->findModel($epitetos, $codigo_deidad, $codigo_autor);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'epitetos' => $model->epitetos, 'codigo_deidad' => $model->codigo_deidad, 'codigo_autor' => $model->codigo_autor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Epitetos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $epitetos
     * @param integer $codigo_deidad
     * @param integer $codigo_autor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($epitetos, $codigo_deidad, $codigo_autor)
    {
        $this->findModel($epitetos, $codigo_deidad, $codigo_autor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Epitetos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $epitetos
     * @param integer $codigo_deidad
     * @param integer $codigo_autor
     * @return Epitetos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($epitetos, $codigo_deidad, $codigo_autor)
    {
        if (($model = Epitetos::findOne(['epitetos' => $epitetos, 'codigo_deidad' => $codigo_deidad, 'codigo_autor' => $codigo_autor])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
