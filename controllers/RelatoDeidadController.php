<?php

namespace app\controllers;

use Yii;
use app\models\RelatoDeidad;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RelatoDeidadController implements the CRUD actions for RelatoDeidad model.
 */
class RelatoDeidadController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RelatoDeidad models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RelatoDeidad::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RelatoDeidad model.
     * @param integer $codigo_relato
     * @param integer $codigo_deidad
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_relato, $codigo_deidad)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_relato, $codigo_deidad),
        ]);
    }

    /**
     * Creates a new RelatoDeidad model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RelatoDeidad();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_relato' => $model->codigo_relato, 'codigo_deidad' => $model->codigo_deidad]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RelatoDeidad model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_relato
     * @param integer $codigo_deidad
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_relato, $codigo_deidad)
    {
        $model = $this->findModel($codigo_relato, $codigo_deidad);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_relato' => $model->codigo_relato, 'codigo_deidad' => $model->codigo_deidad]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RelatoDeidad model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_relato
     * @param integer $codigo_deidad
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_relato, $codigo_deidad)
    {
        $this->findModel($codigo_relato, $codigo_deidad)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RelatoDeidad model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_relato
     * @param integer $codigo_deidad
     * @return RelatoDeidad the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_relato, $codigo_deidad)
    {
        if (($model = RelatoDeidad::findOne(['codigo_relato' => $codigo_relato, 'codigo_deidad' => $codigo_deidad])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
