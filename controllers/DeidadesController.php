<?php

namespace app\controllers;

use Yii;
use app\models\Deidades;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DeidadesController implements the CRUD actions for Deidades model.
 */
class DeidadesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Deidades models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Deidades::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Deidades model.
     * @param integer $codigo_deidad
     * @param integer $codigo_autor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_deidad, $codigo_autor)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_deidad, $codigo_autor),
        ]);
    }

    /**
     * Creates a new Deidades model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Deidades();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_deidad' => $model->codigo_deidad, 'codigo_autor' => $model->codigo_autor]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Deidades model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_deidad
     * @param integer $codigo_autor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_deidad, $codigo_autor)
    {
        $model = $this->findModel($codigo_deidad, $codigo_autor);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_deidad' => $model->codigo_deidad, 'codigo_autor' => $model->codigo_autor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Deidades model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_deidad
     * @param integer $codigo_autor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_deidad, $codigo_autor)
    {
        $this->findModel($codigo_deidad, $codigo_autor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Deidades model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_deidad
     * @param integer $codigo_autor
     * @return Deidades the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_deidad, $codigo_autor)
    {
        if (($model = Deidades::findOne(['codigo_deidad' => $codigo_deidad, 'codigo_autor' => $codigo_autor])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
