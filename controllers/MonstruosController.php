<?php

namespace app\controllers;

use Yii;
use app\models\Monstruos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MonstruosController implements the CRUD actions for Monstruos model.
 */
class MonstruosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Monstruos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Monstruos::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Monstruos model.
     * @param integer $codigo_monstruo
     * @param integer $codigo_autor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_monstruo, $codigo_autor)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_monstruo, $codigo_autor),
        ]);
    }

    /**
     * Creates a new Monstruos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Monstruos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_monstruo' => $model->codigo_monstruo, 'codigo_autor' => $model->codigo_autor]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Monstruos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_monstruo
     * @param integer $codigo_autor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_monstruo, $codigo_autor)
    {
        $model = $this->findModel($codigo_monstruo, $codigo_autor);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_monstruo' => $model->codigo_monstruo, 'codigo_autor' => $model->codigo_autor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Monstruos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_monstruo
     * @param integer $codigo_autor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_monstruo, $codigo_autor)
    {
        $this->findModel($codigo_monstruo, $codigo_autor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Monstruos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_monstruo
     * @param integer $codigo_autor
     * @return Monstruos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_monstruo, $codigo_autor)
    {
        if (($model = Monstruos::findOne(['codigo_monstruo' => $codigo_monstruo, 'codigo_autor' => $codigo_autor])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
